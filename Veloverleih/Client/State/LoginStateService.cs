﻿using Veloverleih.Shared;

namespace Veloverleih.Client.State
{
    public class LoginStateService
    {
        public Users User { get; set; } = Users.Login;


        public event Action OnStateChange;
        
        public void SetValue(Users user)
        {
            User = user;
            NotifyStateChanged();
        }
        
        private void NotifyStateChanged() => OnStateChange?.Invoke();
    }


}
