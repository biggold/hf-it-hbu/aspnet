﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using Veloverleih.Client.Pages;
using Veloverleih.Server.Database;
using Veloverleih.Shared.Models;

namespace Veloverleih.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BicycleController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public BicycleController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Bicycle
        [HttpGet]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<BicycleModel>>> GetBicycle()
        {
            return await _context.Bicycle.ToListAsync();
        }

        // GET: Bicycles
        [HttpGet, Route("Search")]
        [EnableQuery]
        public async Task<IActionResult> Search(string StartDate, string EndDate)
        {
            var bikes = await _context.Bicycle.ToListAsync();
            List<BicycleModel> bicycleModels = new List<BicycleModel>();

            DateTime start = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime end = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            foreach (var bike in bikes)
            {
                if (await CheckTimeSpan(start, end, bike.Id))
                {
                    bicycleModels.Add(bike);
                }
            }

            return Ok(bicycleModels);
        }

        private async Task<bool> CheckTimeSpan(DateTime startDate, DateTime endDate, int bicycleId)
        {
            BuchungModel checkRes = new BuchungModel()
            {
                BicycleId = bicycleId,
                StartDate = startDate,
                EndDate = endDate
            };
            var allBuchung = await _context.Buchung.Where(x => x.BicycleId == checkRes.BicycleId).ToListAsync();
            bool noOverlap = true;

            foreach (var res in allBuchung)
            {
                if (checkRes.StartDate.Date <= res.EndDate.Date && res.StartDate.Date <= checkRes.EndDate.Date)
                {
                    noOverlap = false;
                }
            }
            return noOverlap;
        }

        // GET: api/Bicycle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BicycleModel>> GetBicycleModel(int id)
        {
            var bicycleModel = await _context.Bicycle.Include(b => b.Photos).Include(b => b.Kategorie).FirstOrDefaultAsync(b => b.Id == id);

            if (bicycleModel == null)
            {
                return NotFound();
            }

            return bicycleModel;
        }

        // PUT: api/Bicycle/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBicycleModel(int id, BicycleModel bicycleModel)
        {
            if (id != bicycleModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(bicycleModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BicycleModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Bicycle
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<BicycleModel>> PostBicycleModel(BicycleModel bicycleModel)
        {
            _context.Bicycle.Add(bicycleModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBicycleModel", new { id = bicycleModel.Id }, bicycleModel);
        }

        // DELETE: api/Bicycle/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBicycleModel(int id)
        {
            var bicycleModel = await _context.Bicycle.FindAsync(id);
            if (bicycleModel == null)
            {
                return NotFound();
            }

            _context.Bicycle.Remove(bicycleModel);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool BicycleModelExists(int id)
        {
            return _context.Bicycle.Any(e => e.Id == id);
        }
    }
}
