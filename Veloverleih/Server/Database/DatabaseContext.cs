﻿using Microsoft.EntityFrameworkCore;
using Veloverleih.Shared.Models;

namespace Veloverleih.Server.Database
{
    public class DatabaseContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Filename=:memory:");
            optionsBuilder.UseSqlite("Filename=Database\\app.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BicycleModel>().ToTable("Bicycles");
            modelBuilder.Entity<BuchungModel>().ToTable("Buchungen");
            modelBuilder.Entity<KategorienModel>().ToTable("Kategorien");
            modelBuilder.Entity<PhotoModel>().ToTable("Photos");

            // Photo <-> Bicycle
            modelBuilder.Entity<PhotoModel>()
                .HasOne<BicycleModel>(p => p.Bicycle)
                .WithMany(b => b.Photos)
                .HasForeignKey(p => p.BicycleId);


            // Buchung <-> Bicycle
            modelBuilder.Entity<BicycleModel>()
                .HasMany(b => b.Buchungen)
                .WithOne(b => b.Bicycle)
                .HasForeignKey(b => b.BicycleId);


            // Kategorien <-> Bicycle
            modelBuilder.Entity<KategorienModel>()
                .HasMany(k => k.Bicycles)
                .WithOne(b => b.Kategorie)
                .HasForeignKey(b => b.KategorienId);
        }
        //entities
        public DbSet<BicycleModel> Bicycle { get; set; }
        public DbSet<PhotoModel> Photo { get; set; }
        public DbSet<KategorienModel> Kategorie { get; set; }
        public DbSet<BuchungModel> Buchung { get; set; }
    }
}
