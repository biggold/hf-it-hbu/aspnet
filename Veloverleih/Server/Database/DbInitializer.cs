﻿using Bogus;
using System.Collections.ObjectModel;
using Veloverleih.Shared.Models;

namespace Veloverleih.Server.Database
{
    public static class DbInitializer
    {
        public static void Initialize(DatabaseContext context)
        {
            // Look for any Kategories.
            if (!context.Kategorie.Any())
            {
                IList<KategorienModel> newKategorien = new List<KategorienModel>() {
                    new KategorienModel() { Name = "Dreirad" },
                    new KategorienModel() { Name = "Mountainbike" },
                    new KategorienModel() { Name = "Elektro-Mountainbike" },
                    new KategorienModel() { Name = "Rennvelo" },
                    new KategorienModel() { Name = "Stadtvelo" },
                    new KategorienModel() { Name = "Elektro-Stadtvelo" }
                };
                context.Kategorie.AddRange(newKategorien);
                context.SaveChanges();
            }


            // Look for any bicycles.
            if (!context.Bicycle.Any())
            {
                // Stuff used to get random Kategorie
                Random random = new Random();
                IList <KategorienModel> kategorien = context.Kategorie.ToList();

                // Generating random data
                int bicycleId = 0;
                var bicycleFaker = new Faker<BicycleModel>()
                    .RuleForType(typeof(string), f => f.Random.Word())
                    .RuleFor(b => b.Gaenge, f => f.Random.Int(3, 24))
                    .RuleFor(b => b.Reifengroesse, f => f.Random.Int(3, 24))
                    .RuleFor(b => b.Ort, f => f.Address.City())
                    .RuleFor(b => b.Thumbnail, (f) => f.Image.LoremFlickrUrl(320, 240, "bicycle,bicycles,velo,velos", false, false))
                    .RuleFor(b => b.Photos, (f) =>
                    {
                        ICollection<PhotoModel> images = new Collection<PhotoModel>();
                        for (int i = 0; i < f.Random.Int(3, 6); i++)
                        {
                            images.Add(new PhotoModel()
                            {
                                URL = f.Image.LoremFlickrUrl(320, 240, "bicycle,bicycles,velo,velos", false, false)
                            });
                        }
                        return images;
                    })
                    .RuleFor(b => b.KategorienId, (f) =>
                    {
                        // select a random category
                        int index = random.Next(kategorien.Count);
                        return kategorien[index].Id;
                    });
                var bicycleList = bicycleFaker.Generate(10);


                context.Bicycle.AddRange(bicycleList);
                context.SaveChanges();
            }
        }
    }
}
