﻿using Veloverleih.Shared.Models;

namespace Veloverleih.Shared
{ 
    public class BicycleFilter
    {
        private DateTime startTime = DateTime.Now;
        public DateTime StartTime
        {
            set
            {
                startTime = value;
                // We should update endtime as well to not have it in the past
                endTime = value;
                DateRangeIsSet = true;
            }
            get => startTime;
        }

        private DateTime endTime = DateTime.Now;
        public DateTime EndTime
        {
            set
            {
                endTime = value;
                // If endtime before starttime we should set it to the same value
                if (endTime < startTime ) endTime = startTime;
            }
            get => endTime;
        }

        public bool DateRangeIsSet { get; set; } = false;


        private KategorienModel kategorie;
        public KategorienModel Kategorie
        {
            set
            {
                kategorie = value;
            }
            get => kategorie;
        }


        private string name;
        public string Name
        {
            set
            {
                name = value;
            }
            get => name;
        }

        private string marke;
        public string Marke
        {
            set
            {
                marke = value;
            }
            get => marke;
        }
    }
}
