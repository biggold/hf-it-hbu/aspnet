﻿
namespace Veloverleih.Shared.Models
{
   

    public class KategorienModel
    {
        public int Id{ get; set; }
        public string Name { get; set; }

        public ICollection<BicycleModel> Bicycles { get; set; }
    }
}
