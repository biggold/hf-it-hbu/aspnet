﻿namespace Veloverleih.Shared.Models
{
    public class BicycleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Marke { get; set; }
        public float Preis { get; set; }

        public string Thumbnail { get; set; }

        public string Ort { get; set; }

        public ICollection<PhotoModel> Photos { get; set; }

        public int Gaenge { get; set; }
        public int Reifengroesse { get; set; }

        public ICollection<BuchungModel> Buchungen { get; set; }

        public int KategorienId { get; set; }
        public KategorienModel Kategorie { get; set; }

    }
}
