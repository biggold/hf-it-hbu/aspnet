﻿namespace Veloverleih.Shared.Models
{
    public class BuchungModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Now;
        public DateTime EndDate { get; set; } = DateTime.Now;

        public int BicycleId { get; set; }
        public BicycleModel Bicycle { get; set; }

        // TODO: Buchungs person
    }
}
