﻿namespace Veloverleih.Shared.Models
{
    public class PhotoModel
    {
        public int Id { get; set; }
        public string URL { get; set; }

        public BicycleModel Bicycle { get; set; }
        public int BicycleId { get; set; }
    }
}
